'use strict';
let game;
$("#start_button").click(function() {
	let bombNumbers = []; let bombNumbersLength = 10; while(bombNumbersLength--) bombNumbers[bombNumbersLength] = false;
	
	let str = $("#birth_date").val();
	for (let i = 0; i < str.length; i++)
		switch(str.charAt(i)) {
			case "0": bombNumbers[0] = true; break;
			case "1": bombNumbers[1] = true; break;
			case "2": bombNumbers[2] = true; break;
			case "3": bombNumbers[3] = true; break;
			case "4": bombNumbers[4] = true; break;
			case "5": bombNumbers[5] = true; break;
			case "6": bombNumbers[6] = true; break;
			case "7": bombNumbers[7] = true; break;
			case "8": bombNumbers[8] = true; break;
			case "9": bombNumbers[9] = true; break;
		}
	$("#startup_elements").hide();
	$("#game_elements").show();
	game = new Game();
	game.init(Generator.generateBombs(bombNumbers));
	document.querySelector('meta[name="viewport"]').content = "width=" + document.documentElement.clientWidth; //Zoom out to fit all content to viewport
	game.animateNewGame();
});

$("#button_try_again").click(function() {
	game.tryAgain();
});

$("#reveal_all").click(function() {
	game.revealAll();
});

Preloader.preload(Resources.getResourcesList());
