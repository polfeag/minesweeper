'use strict';

// Assigns CSS paths to images variables
// and preloads the images.
class Preloader {
    static preload(arrayOfImages) {
        // String[]
        let prefetchLinksHtml = [];
        // String[]
        let cssVariablesAssignHtml = [];
        $.each(arrayOfImages, function (key) {
            let imageName = arrayOfImages[key];

            let prefetchLinkPath = Config.IMAGES_PATH_PREFETCH + imageName;
            let cssVariablePath = Config.IMAGES_PATH_CSS + imageName;

            let prefetchLinkHtml = `<link rel="prefetch" as="image" href="${prefetchLinkPath}">`;
            let cssVariableAssignHtml = `--tile-bg-image-${key}:url("${cssVariablePath}");`;

            prefetchLinksHtml.push(prefetchLinkHtml);
            cssVariablesAssignHtml.push(cssVariableAssignHtml);
        });

        let value = ($(document.documentElement).attr('style') ?? '')
            + cssVariablesAssignHtml.join('');
        $(document.documentElement).attr('style', value);
        value = prefetchLinksHtml.join('');
        $('head').append(value);
    }
}
