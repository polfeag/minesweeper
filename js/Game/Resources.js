'use strict';

class Resources {
    static getResourcesList()
    {
        return {
            "opened":                  "open.png",
            "undiscovered":            "undiscovered.svg",
            "undiscovered-cracked":    "undiscovered_cracked.svg",
            "bomb":                    "bomb.svg",
            "bomb-cracked":            "bomb_cracked.svg",
            "side-left":               "side_left.svg",
            "side-right":              "side_right.svg",
            "side-top":                "side_top.svg",
            "side-bottom":             "side_bottom.svg",
            "one":                     "one.svg",
            "two":                     "two.svg",
            "three":                   "three.svg",
            "four":                    "four.svg",
            "five":                    "five.svg",
            "six":                     "six.svg",
            "seven":                   "seven.svg",
            "eight":                   "eight.svg"
        };
    }
}
