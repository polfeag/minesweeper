class Config
{
    static rows = 18;
    static columns = 18;
    static DOUBLE_CLICK_TIME = 500;

    static IMAGES_PATH_CSS = '../img/';
    static IMAGES_PATH_PREFETCH = 'img/';
}
