'use strict';

class Cell {
	static VISUAL_STATE = Object.freeze({"undiscovered":1, "open":2, "bomb":3, "one":4, "two":5, "three":6, "four":7, "five":8, "six":9, "seven":10, "eight":11, "bomb_cracked":12, "undiscovered_cracked":13, "only_direction":14, "only_bombs":15});
	_visualState;
	isMine;
	x;
	y;
	htmlCell;
	htmlCell_Inner;
	htmlCell_Front;
	htmlCell_Back;

	playfield;
	constructor(x, y, isMine, playfield) {
		this.playfield = playfield;
		this.x = x;
		this.y = y;
		let game_cell = document.createElement("div");
		game_cell.classList.add("game-cell");
		$("#playfield").append(game_cell);
		this.htmlCell = game_cell;

		let game_cell_inner = document.createElement("div");
		game_cell_inner.classList.add("game-cell-inner");
		game_cell.append(game_cell_inner);
		this.htmlCell_Inner = game_cell_inner;

		let game_cell_front = document.createElement("div");
		game_cell_front.classList.add("game-cell-front");
		game_cell_inner.append(game_cell_front);
		this.htmlCell_Front = game_cell_front;

		let game_cell_back = document.createElement("div");
		game_cell_back.classList.add("game-cell-back");
		this.htmlCell_Inner.append(game_cell_back);
		this.htmlCell_Back = game_cell_back;

		let undiscovered_image = document.createElement("div"); //img
		undiscovered_image.classList.add("cell_undiscovered");
		this.htmlCell_Front.append(undiscovered_image);

		this.isMine = isMine;
		this._visualState = Cell.VISUAL_STATE.undiscovered;
	}
	get visualState() {
		return this._visualState;
	}
	set visualState(value) {
		let isFlip = true;
		let isDiagonal = true;
		let isDirect = true;
		this._visualState = value;
		let new_image = document.createElement("div"); //img
		switch(value) {
			case Cell.VISUAL_STATE.undiscovered:
			{
				this.htmlCell_Front.innerHTML = "";

				let undiscovered_image = document.createElement("div"); //img
				undiscovered_image.classList.add("cell_undiscovered");
				this.htmlCell_Front.append(undiscovered_image);

				this.htmlCell_Inner.classList.remove("game-cell-flip");
				return;
			}
			case Cell.VISUAL_STATE.only_direction:
			{
				if(this.isMine){
					new_image.classList.add("cell_bomb");
				} else {
					value=this.lookUnderhood();
					isDiagonal=false;
				}
			}
				break;
			case Cell.VISUAL_STATE.only_bombs:
			{
				if(this.isMine){
					new_image.classList.add("cell_bomb");
				}
			}
				break;
			case Cell.VISUAL_STATE.open:                new_image.classList.add("cell_open"); break;
			case Cell.VISUAL_STATE.bomb:                new_image.classList.add("cell_bomb"); break;
			case Cell.VISUAL_STATE.bomb_cracked:
			{
				let img = document.createElement("div"); //img
				img.classList.add("cell_bomb_cracked");
				this.htmlCell_Front.innerHTML = "";
				this.htmlCell_Front.append(img);
				isFlip = false;
			}
				break;
			case Cell.VISUAL_STATE.undiscovered_cracked:
			{
				let img = document.createElement("div"); //img
				img.classList.add("cell_undiscovered_cracked");
				this.htmlCell_Front.innerHTML = "";
				this.htmlCell_Front.append(img);
				isFlip = false;
			}
				break;
			case Cell.VISUAL_STATE.one:                 new_image.classList.add("cell_one"); break;
			case Cell.VISUAL_STATE.two:                 new_image.classList.add("cell_two"); break;
			case Cell.VISUAL_STATE.three:               new_image.classList.add("cell_three"); break;
			case Cell.VISUAL_STATE.four:                new_image.classList.add("cell_four"); break;
			case Cell.VISUAL_STATE.five:                new_image.classList.add("cell_five"); break;
			case Cell.VISUAL_STATE.six:                 new_image.classList.add("cell_six"); break;
			case Cell.VISUAL_STATE.seven:               new_image.classList.add("cell_seven"); break;
			case Cell.VISUAL_STATE.eight:               new_image.classList.add("cell_eight"); break;
		}

		this.htmlCell_Back.innerHTML = "";

		switch(value){
			case Cell.VISUAL_STATE.one:
			case Cell.VISUAL_STATE.two:
			case Cell.VISUAL_STATE.three:
			case Cell.VISUAL_STATE.four:
			case Cell.VISUAL_STATE.five:
			case Cell.VISUAL_STATE.six:
			case Cell.VISUAL_STATE.seven:
			case Cell.VISUAL_STATE.eight:
				let nextDirect = [];
				let nextDiagonal = [];
				let prevDirect = [];
				let prevDiagonal = [];
				let elementDirect = [];
				let elementDiagonal = [];
				let isIndicator = [];


				if(isDiagonal)
					this.doAdjacentDiagonal(function(cell, index){
						elementDiagonal[index] = cell;
					});
				if(isDirect)
					this.doAdjacentDirect(function(cell, index){
						elementDirect[index] = cell;
					});
				prevDiagonal[0] = elementDiagonal[3];
				prevDirect[0] = elementDirect[3];
				nextDiagonal[3] = elementDiagonal[0];
				nextDirect[3] = elementDirect[0];

				nextDiagonal[0] = elementDiagonal[1];
				nextDiagonal[1] = elementDiagonal[2];
				nextDiagonal[2] = elementDiagonal[3];
				nextDirect[0] = elementDirect[1];
				nextDirect[1] = elementDirect[2];
				nextDirect[2] = elementDirect[3];
				prevDiagonal[1] = elementDiagonal[0];
				prevDiagonal[2] = elementDiagonal[1];
				prevDiagonal[3] = elementDiagonal[2];
				prevDirect[1] = elementDirect[0];
				prevDirect[2] = elementDirect[1];
				prevDirect[3] = elementDirect[2];


				let _isMine = (cell) => cell !== undefined && cell.isMine;
				this.doAdjacentDirect(function(cell, index){
					if(cell.isMine){
						isIndicator[index] = true;
					} else if(
						_isMine(elementDiagonal[index]) &&
						_isMine(nextDiagonal[index]) &&
						!_isMine(prevDirect[index]) && //remove this condition to change hints behavior
						!_isMine(nextDirect[index]) //remove this condition to change hints behavior
					){
						isIndicator[index] = true;
					} else if(
						_isMine(elementDiagonal[index]) &&
						!_isMine(prevDiagonal[index]) &&
						!_isMine(nextDiagonal[index]) &&
						!_isMine(prevDirect[index])
					) {
						if(Math.floor(Math.random() * 2) === 0)
							isIndicator[index] = true;
						else
							isIndicator[index === 0 ? 3 : index - 1] = true;
					}
				});

				if(isIndicator[0]){
					let sideElement = document.createElement("div");
					sideElement.classList.add("side-indicator-left");
					this.htmlCell_Back.append(sideElement);
				}
				if(isIndicator[1]){
					let sideElement = document.createElement("div");
					sideElement.classList.add("side-indicator-top");
					this.htmlCell_Back.append(sideElement);
				}
				if(isIndicator[2]){
					let sideElement = document.createElement("div");
					sideElement.classList.add("side-indicator-right");
					this.htmlCell_Back.append(sideElement);
				}
				if(isIndicator[3]){
					let sideElement = document.createElement("div");
					sideElement.classList.add("side-indicator-bottom");
					this.htmlCell_Back.append(sideElement);
				}

		}
		this.htmlCell_Back.append(new_image);
		if(isFlip)
			this.htmlCell_Inner.classList.add("game-cell-flip");
	}
	doAdjacent(action){ //start from left-bottom
		let x, y, index = 0;
		x = this.x-1; y = this.y+1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x-1; y = this.y;   if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x-1; y = this.y-1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x; y = this.y-1;   if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y-1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y;   if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y+1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x; y = this.y+1;   if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
	}
	doAdjacentDirect(action){ //start from left
		let x, y, index = 0;
		x = this.x-1; y = this.y; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x; y = this.y-1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x; y = this.y+1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
	}
	doAdjacentDiagonal(action){ //start from left-bottom
		let x, y, index = 0;
		x = this.x-1; y = this.y+1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x-1; y = this.y-1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y-1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
		x = this.x+1; y = this.y+1; if(this.playfield.isCellPosValid(x, y)) action(this.playfield.cells[x][y], index); index++;
	}
	lookUnderhood(){
		if(this.isMine)
			return Cell.VISUAL_STATE.bomb;
		let minesCount = 0;
		this.doAdjacent(function(cell){
			if(cell.isMine)
				minesCount++;
		});
		let visualState;
		switch(minesCount) {
			case 0:
				visualState = Cell.VISUAL_STATE.open;
				break;
			case 1: visualState = Cell.VISUAL_STATE.one; break;
			case 2: visualState = Cell.VISUAL_STATE.two; break;
			case 3: visualState = Cell.VISUAL_STATE.three; break;
			case 4: visualState = Cell.VISUAL_STATE.four; break;
			case 5: visualState = Cell.VISUAL_STATE.five; break;
			case 6: visualState = Cell.VISUAL_STATE.six; break;
			case 7: visualState = Cell.VISUAL_STATE.seven; break;
			case 8: visualState = Cell.VISUAL_STATE.eight; break;
		}
		return visualState;
	}
}

class Input{
	static INPUT_TYPE = Object.freeze({"mine":1, "cell":2});
	_prevTapTimestamp = 0;
	_prevTapObject = null;
	_prevTimer = null;
	_prevClickTimestamp = null;
	_sensorTap(e, cell, actionFunc){
		if(e.sourceCapabilities.firesTouchEvents){
			if(e.timeStamp - this._prevClickTimestamp < Config.DOUBLE_CLICK_TIME && cell === this._prevTapObject){
				this._prevClickTimestamp = 0;
				clearTimeout(this._prevTimer);
				actionFunc(cell, Input.INPUT_TYPE.mine);
			}else{
				this._prevTimer = setTimeout(
					() => actionFunc(cell, Input.INPUT_TYPE.cell)
					,Config.DOUBLE_CLICK_TIME);
			}
			this._prevTapObject = cell;
			this._prevClickTimestamp = e.timeStamp;
		}
	}
	_mouseLeftClick(e, cell, actionFunc){
		if(!e.sourceCapabilities.firesTouchEvents) {
			actionFunc(cell, Input.INPUT_TYPE.cell);
		}
	}
	_mouseRightClick(e, cell, actionFunc){
		e.preventDefault();
		if(!e.sourceCapabilities.firesTouchEvents) {
			actionFunc(cell, Input.INPUT_TYPE.mine);
		}
	}
	addElementToActivate(element, actionFunc){
		element.htmlCell.addEventListener(
			"click"
			,(e) => this._sensorTap(e, element, actionFunc));//TAP
		element.htmlCell.addEventListener(
			"click"
			,(e) => this._mouseLeftClick(e, element, actionFunc));//LEFT CLICK
		element.htmlCell.addEventListener(
			"contextmenu"
			,(e) => this._mouseRightClick(e, element, actionFunc));//RIGHT CLICK
	}
}

class Game {
	input;
	playfield;
	disableInput;

	init(mines) {
		let this_ = this;
		this.input = new Input();
		this.playfield = new playfield(mines);

		this.playfield.doAllCells(function (cell) {
			this_.input.addElementToActivate(cell, function (cell, actionType) {
				if (this_.disableInput) {
					return;
				}
				let result;
				switch (actionType) {
					case Input.INPUT_TYPE.cell:
						result = this_.playfield.tryOpenAsCell(cell);
						break;
					case Input.INPUT_TYPE.mine:
						result = this_.playfield.tryOpenAsMine(cell);
						break;
				}
				if (result === false){
					this_.disableInput = true;
				}
			});
		});
	}

	animateNewGame() {
		let this_ = this;
		this.disableInput = true;
		let delayRow = 100;
		for (let y = 0; y < Config.rows; y++) {
			delayRow += 70;
			let delayColumn = 0;
			for (let x = 0; x < Config.columns; x++) {
				let cell = this.playfield.cells[x][y];
				delayColumn += 30;
				setTimeout(function () {
						this_.playfield.undiscover(cell);
					}
					, delayRow + delayColumn);
			}
		}
		let delayFullFirstStage = delayRow + (30 * Config.columns) * 2;
		setTimeout(function () {
			this_.disableInput = false;
			for (let x = 0; x < Config.columns; x++) {
				this_.playfield.open(this_.playfield.cells[x][0]);
			}
		}, delayFullFirstStage);
	}

	tryAgain() {
		let this_ = this;
		this.disableInput = false;
		this.playfield.doAllCells(function (cell) {
			this_.playfield.undiscover(cell);
			if(cell.y===0){
				for(let i = 0; i < Config.columns; i++){
					this_.playfield.open(cell);
				}
			}
		});
	}

	revealAll() {
		let this_ = this;
		this.playfield.doAllCells(function (cell) {
			this_.playfield.openSingle(cell);
			setTimeout(function(){ cell.visualState = Cell.VISUAL_STATE.only_direction; }, 5000);
			setTimeout(function(){ cell.visualState = Cell.VISUAL_STATE.only_bombs; }, 10000);
		});
	}
}

class playfield {
	// [x][y]
	cells = [];
	open(cell){
		if (cell.isMine) {
			this.tryOpenAsMine(cell);
		} else {
			this.tryOpenAsCell(cell);
		}
	}
	openSingle(cell){
		cell.visualState=cell.lookUnderhood();
	}
	undiscover(cell){
		cell.htmlCell_Inner.classList.remove("game-cell-flip-game-begin");
		cell.visualState = Cell.VISUAL_STATE.undiscovered;
	}
	tryOpenAsCell(cell){
		if(cell.isMine){
			cell.visualState = Cell.VISUAL_STATE.bomb_cracked;
			return false;
		}
		let visualState = cell.lookUnderhood();
		cell.visualState = visualState;

		if(visualState !== Cell.VISUAL_STATE.open)
			return;
		let doAdjacentFunc = function(cell) {
			cell.doAdjacent(function(cell){
				if(cell.visualState === Cell.VISUAL_STATE.undiscovered){
					let visualState = cell.lookUnderhood();
					cell.visualState= visualState;
					if(visualState === Cell.VISUAL_STATE.open){
						doAdjacentFunc(cell);
					}
				}
			});
		};
		doAdjacentFunc(cell);
	}
	tryOpenAsMine(cell){
		if(!cell.isMine){
			cell.visualState = Cell.VISUAL_STATE.undiscovered_cracked;
			return false;
		}
		let visualState = cell.lookUnderhood();
		if(cell.isMine && cell.visualState === Cell.VISUAL_STATE.undiscovered){
			cell.visualState = visualState;
			let doAdjacentFunc = function(cell) {
				cell.doAdjacentDirect(function(cell){
					if(cell.visualState === Cell.VISUAL_STATE.undiscovered){
						let visualState = cell.lookUnderhood();
						if(visualState === Cell.VISUAL_STATE.bomb){
							cell.visualState = visualState;
							doAdjacentFunc(cell);
						}
					}
				});
			};
			doAdjacentFunc(cell);
		}
	}
	doAllCells(action){
		for (let y = Config.rows-1; y >= 0; y--) {
			for (let x = 0; x < Config.columns; x++) {
				let cell = this.cells[x][y];
				action(cell);
			}
		}
	}
	isCellPosValid(x, y){
		return (x >= 0 && x < this.cells.length && y >= 0 && y < this.cells[x].length);
	}
	constructor(mines) {
		this.cells = [];
		document.documentElement.style.setProperty('--grid-container-columns', "30px ".repeat(Config.columns));
		document.documentElement.style.setProperty('--grid-container-rows', "30px ".repeat(Config.rows));
		for (let x = 0; x < Config.columns; x++){
			this.cells[x] = [];
		}
		for (let y = 0; y < Config.rows; y++) {
			for (let x = 0; x < Config.columns; x++) {
				let cell = new Cell(x, y, mines[x][y], this);
				this.cells[x][y] = cell;
				cell.htmlCell_Inner.classList.add("game-cell-flip-game-begin");
			}
		}
	}
}
