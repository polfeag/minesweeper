'use strict'

class Generator {
    static generateBombs(numbers)
    {
        let numberMatrix = [];
        numberMatrix[ 0] = [1,2,3,4,5,6,7,8,9 ,9,8,7,6,5,4,3,2,1];
        numberMatrix[ 1] = [2,4,6,8,0,2,3,6,8 ,8,6,4,2,0,8,6,4,2];
        numberMatrix[ 2] = [3,6,9,2,5,8,1,4,7 ,7,4,1,8,5,2,9,6,3];
        numberMatrix[ 3] = [4,8,2,6,0,4,8,2,6 ,6,2,8,4,0,6,2,8,4];
        numberMatrix[ 4] = [5,0,5,0,5,0,5,0,5 ,5,0,5,0,5,0,5,0,5];
        numberMatrix[ 5] = [6,2,8,4,0,6,2,8,4 ,4,8,2,6,0,4,8,2,6];
        numberMatrix[ 6] = [7,4,1,8,5,2,9,6,3 ,3,6,9,2,5,8,1,3,7];
        numberMatrix[ 7] = [8,6,4,2,0,8,6,4,2 ,2,4,6,8,0,2,4,6,8];
        numberMatrix[ 8] = [9,8,7,6,5,4,3,2,1 ,1,2,3,4,5,6,7,8,9];

        numberMatrix[ 9] = [9,8,7,6,5,4,3,2,1 ,1,2,3,4,5,6,7,8,9];
        numberMatrix[10] = [8,6,4,2,0,8,6,4,2 ,2,4,6,8,0,2,4,6,8];
        numberMatrix[11] = [7,3,1,8,5,2,9,6,3 ,3,6,9,2,5,8,1,4,7];
        numberMatrix[12] = [6,2,8,4,0,6,2,8,4 ,4,8,2,6,0,4,8,2,6];
        numberMatrix[13] = [5,0,5,0,5,0,5,0,5 ,5,0,5,0,5,0,5,0,5];
        numberMatrix[14] = [4,8,2,6,0,4,8,2,6 ,6,2,8,4,0,6,2,8,4];
        numberMatrix[15] = [3,6,9,2,5,8,1,4,7 ,7,4,1,8,5,2,9,6,3];
        numberMatrix[16] = [2,4,6,8,0,2,4,6,8 ,8,6,3,2,0,8,6,4,2];
        numberMatrix[17] = [1,2,3,4,5,6,7,8,9 ,9,8,7,6,5,4,3,2,1];
        let resultMatrix = [];
        for (let x = 0; x < numberMatrix.length; x++) {
            resultMatrix[x] = [];
            for (let y = 0; y < numberMatrix[x].length; y++) {
                resultMatrix[x][y] = !!numbers[numberMatrix[x][y]];
            }
        }
        return resultMatrix;
    }
}
